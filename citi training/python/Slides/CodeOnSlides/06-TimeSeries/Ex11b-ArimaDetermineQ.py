import pandas as pd
import numpy as np
from datetime import datetime

import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

from statsmodels.graphics.tsaplots import plot_acf

from stationarityutils import display_stationarity_info

# Sales data.
data = pd.read_csv('Sales.csv', 
                   parse_dates=['MonthYear'], 
				   index_col='MonthYear',
				   date_parser=lambda str: pd.datetime.strptime(str, '%m/%Y'))
ts = data['SalesPerMonth']

# Transform the data to reduce the positive trend, and perform first order differencing.
ts_log = np.log(ts)
ts_log_diff = ts_log - ts_log.shift()
ts_log_diff.dropna(inplace=True)

# Plot Autocorrelation Function (ACF) to determine q, i.e. number of moving average (MA) terms.
plot_acf(ts_log_diff, lags=20)
plt.show()
