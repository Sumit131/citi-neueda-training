import pandas as pd
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt

import random
random.seed(5)

# Imports for plotting datetimes on graphs.
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

def plot_rolling_statistics(message, ts) :

    # Determine rolling statistics.
    rolling = ts.rolling(window=12)   # Get a Rolling object, which enables us to determine rolling values over a specified window size (12 months here).
    rolling_mean = rolling.mean()     # Calculate the rolling mean, over 12-month windows.
    rolling_std  = rolling.std()      # Calculate the rolling standard deviation, over 12-month windows. 

    # Plot rolling statistics.
    orig = plt.plot(ts, color='blue', label='Original')
    mean = plt.plot(rolling_mean, color='orange', label='Rolling mean')
    std  = plt.plot(rolling_std,  color='purple', label = 'Rolling std dev')

    plt.legend()
    plt.title(message)
    plt.show()


# Stationary data.
stationary_data = []
for i in range(0,101):
    stationary_data.append(random.random())
ts = pd.Series(stationary_data)

plot_rolling_statistics("Rolling stats, stationary data", ts)


# Non-stationary data.
non_stationary_data = []
for i in range(0,101):
    non_stationary_data.append(random.random() + i * 0.02)
ts = pd.Series(non_stationary_data)

plot_rolling_statistics("Rolling stats, non-stationary data", ts)


# Sales data.
data = pd.read_csv('Sales.csv', 
                   parse_dates=['MonthYear'], 
				   index_col='MonthYear',
				   date_parser=lambda str: pd.datetime.strptime(str, '%m/%Y'))
ts = data['SalesPerMonth']

plot_rolling_statistics("Rolling stats, sales data", ts)
