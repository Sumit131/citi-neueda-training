import pandas as pd
import numpy as np
from datetime import datetime

import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.arima_model import ARIMA

from stationarityutils import display_stationarity_info

# Sales data.
data = pd.read_csv('Sales.csv', 
                   parse_dates=['MonthYear'], 
				   index_col='MonthYear',
				   date_parser=lambda str: pd.datetime.strptime(str, '%m/%Y'))
ts = data['SalesPerMonth']
print("ts", ts.head())

# Transform the data to reduce the positive trend, and perform first order differencing.
ts_log = np.log(ts)
ts_log_diff = ts_log - ts_log.shift()
ts_log_diff.dropna(inplace=True)

# Use the ARIMA model (with p=2, d=1, q=2).
model = ARIMA(ts_log, order=(2, 1, 2))  
results = model.fit(disp=0)  

# For convenience, copy predicted results into a separate series. 
predictions_log_diff = pd.Series(results.fittedvalues, copy=True)
print('\npredictions_log_diff\n', predictions_log_diff.head())

# Calculate cumulative sums at each position.
predictions_log_diff_cumsum = predictions_log_diff.cumsum()
print('\npredictions_log_diff_cumsum\n', predictions_log_diff_cumsum.head())

# Create a time series containing term 0 in each position.
term0 = pd.Series(ts_log.iloc[0], index=ts_log.index)
print('\nterm0\n', term0.head())

# Add term 0 to the cumulative sums.
predictions_log = term0.add(predictions_log_diff_cumsum, fill_value=0)
print('\npredictions_log\n', predictions_log.head())

# Do an exp() to get back anti-log values. 
predictions = np.exp(predictions_log)

# Calculate the Root Mean Square Error (RMSE).
rmse = np.sqrt(sum((predictions - ts) ** 2) / len(ts))

# Plot the actual and predicted data, plus the root mean square error.
plt.plot(ts)
plt.plot(predictions)
plt.title('RMSE: %.4f'% rmse)
plt.show()