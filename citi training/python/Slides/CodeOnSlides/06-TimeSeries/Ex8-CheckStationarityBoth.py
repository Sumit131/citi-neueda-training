import pandas as pd
import numpy as np
from datetime import datetime

from stationarityutils import display_stationarity_info

import random
random.seed(5)

# Stationary data.
stationary_data = []
for i in range(0,101):
    stationary_data.append(random.random())
ts = pd.Series(stationary_data)
display_stationarity_info("Stationary data", ts)

# Non-stationary data.
non_stationary_data = []
for i in range(0,101):
    non_stationary_data.append(random.random() + i * 0.02)
ts = pd.Series(non_stationary_data)
display_stationarity_info("Non-stationary data", ts)

# Sales data.
data = pd.read_csv('Sales.csv', 
                   parse_dates=['MonthYear'], 
				   index_col='MonthYear',
				   date_parser=lambda str: pd.datetime.strptime(str, '%m/%Y'))
ts = data['SalesPerMonth']
display_stationarity_info("Sales data", ts)
