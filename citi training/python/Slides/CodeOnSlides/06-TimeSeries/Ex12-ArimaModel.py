import pandas as pd
import numpy as np
from datetime import datetime

import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

from arimautils import fit_and_plot_ARIMA_model

# Sales data.
data = pd.read_csv('Sales.csv', 
                   parse_dates=['MonthYear'], 
				   index_col='MonthYear',
				   date_parser=lambda str: pd.datetime.strptime(str, '%m/%Y'))
ts = data['SalesPerMonth']

# Transform the data to reduce the positive trend, and perform first order differencing.
ts_log = np.log(ts)
ts_log_diff = ts_log - ts_log.shift()
ts_log_diff.dropna(inplace=True)

# Plot various ARIMA graphs.
fit_and_plot_ARIMA_model(ts_log, ts_log_diff, 2, 1, 0, 311)
fit_and_plot_ARIMA_model(ts_log, ts_log_diff, 0, 1, 2, 312)
fit_and_plot_ARIMA_model(ts_log, ts_log_diff, 2, 1, 2, 313)

# Display the graphs.
plt.tight_layout()
plt.show()

