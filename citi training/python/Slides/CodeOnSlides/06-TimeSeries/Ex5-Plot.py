import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

# Load data, and convert the MonthYear column into a DateTime type (for easier date-related processing).
data = pd.read_csv('Sales.csv', 
                   parse_dates=['MonthYear'], 
				   index_col='MonthYear',
				   date_parser=lambda str: pd.datetime.strptime(str, '%m/%Y'))

# Get the SalesPerMonth column as a Pandas Series object.
ts = data['SalesPerMonth']

# Plot the time series.
plt.plot(ts)
plt.show()
