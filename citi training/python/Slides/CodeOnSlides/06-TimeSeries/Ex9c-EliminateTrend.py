import pandas as pd
import numpy as np
from datetime import datetime

from stationarityutils import display_stationarity_info

# Sales data.
data = pd.read_csv('Sales.csv', 
                   parse_dates=['MonthYear'], 
				   index_col='MonthYear',
				   date_parser=lambda str: pd.datetime.strptime(str, '%m/%Y'))
ts = data['SalesPerMonth']

# Transform the data to reduce the positive trend (e.g. take the log(), to penalize higher values). 
ts_log = np.log(ts)

# Calculate ewma.
ewma = pd.DataFrame.ewm(ts_log, halflife=8).mean()

# Subtract ewma from data points.
ts_log_ewma_diff = ts_log - ewma

display_stationarity_info('Subtracted ewma', ts_log_ewma_diff)
