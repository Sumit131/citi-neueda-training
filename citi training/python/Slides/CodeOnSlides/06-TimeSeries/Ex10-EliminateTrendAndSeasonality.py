import pandas as pd
import numpy as np
from datetime import datetime

from stationarityutils import display_stationarity_info

# Sales data.
data = pd.read_csv('Sales.csv', 
                   parse_dates=['MonthYear'], 
				   index_col='MonthYear',
				   date_parser=lambda str: pd.datetime.strptime(str, '%m/%Y'))
ts = data['SalesPerMonth']

# Transform the data to reduce the positive trend (e.g. take the log(), to penalize higher values). 
ts_log = np.log(ts)

# Perform first order differencing.
ts_log_diff1 = ts_log - ts_log.shift()
ts_log_diff1.dropna(inplace=True)
display_stationarity_info('First order differencing', ts_log_diff1)

# Perform second order differencing.
ts_log_diff2 = ts_log_diff1 - ts_log_diff1.shift()
ts_log_diff2.dropna(inplace=True)
display_stationarity_info('Second order differencing', ts_log_diff2)

# Perform third order differencing.
ts_log_diff3 = ts_log_diff2 - ts_log_diff2.shift()
ts_log_diff3.dropna(inplace=True)
display_stationarity_info('Third order differencing', ts_log_diff3)
