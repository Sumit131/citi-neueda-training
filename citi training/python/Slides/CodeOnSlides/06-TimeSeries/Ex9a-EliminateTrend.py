import pandas as pd
import numpy as np
from datetime import datetime

from stationarityutils import display_stationarity_info

# Sales data.
data = pd.read_csv('Sales.csv', 
                   parse_dates=['MonthYear'], 
				   index_col='MonthYear',
				   date_parser=lambda str: pd.datetime.strptime(str, '%m/%Y'))
ts = data['SalesPerMonth']

# Transform the data to reduce the positive trend (e.g. take the log(), to penalize higher values). 
ts_log = np.log(ts)
display_stationarity_info('Log sales data', ts_log)
