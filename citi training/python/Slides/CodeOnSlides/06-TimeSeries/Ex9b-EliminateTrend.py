import pandas as pd
import numpy as np
from datetime import datetime

from stationarityutils import display_stationarity_info

# Sales data.
data = pd.read_csv('Sales.csv', 
                   parse_dates=['MonthYear'], 
				   index_col='MonthYear',
				   date_parser=lambda str: pd.datetime.strptime(str, '%m/%Y'))
ts = data['SalesPerMonth']

# Transform the data to reduce the positive trend (e.g. take the log(), to penalize higher values). 
ts_log = np.log(ts)

# Calculate moving averages.
ma = ts_log.rolling(window=12).mean()

# Subtract moving averages from data points.
ts_log_ma_diff = ts_log - ma

# The first 12 values will be NaN, so drop these from the dataset. 
ts_log_ma_diff.dropna(inplace=True)

display_stationarity_info('Subtracted moving average', ts_log_ma_diff)
