import pandas as pd
import numpy as np
from datetime import datetime

import random
random.seed(5)

# Imports for statstools.
from statsmodels.tsa.stattools import adfuller

def print_adResult(message, adf_result) :
    print('\n%s' % message)
    print('-------------------------------------------')
    print('ADF Statistic    \t %g' % adf_result[0])
    print('p-value          \t %g' % adf_result[1])
    
    for k,v in adf_result[4].items() :
        print('Critical value (%s) \t %s' % (k, v))


# Stationary data.
stationary_data = []
for i in range(0,101):
    stationary_data.append(random.random())

adf_result = adfuller(stationary_data)
print_adResult("ADF results for stationary data", adf_result)


# Non-stationary data.
non_stationary_data = []
for i in range(0,101):
    non_stationary_data.append(random.random() + i * 0.02)

adf_result = adfuller(non_stationary_data)
print_adResult("ADF results for non-stationary data", adf_result)


# Sales data.
data = pd.read_csv('Sales.csv', 
                   parse_dates=['MonthYear'], 
				   index_col='MonthYear',
				   date_parser=lambda str: pd.datetime.strptime(str, '%m/%Y'))
ts = data['SalesPerMonth']

adf_result = adfuller(ts)
print_adResult("ADF results for sales data", adf_result)


