import pandas as pd

df1 = pd.DataFrame({
    'name': ['Andy', 'Jayne', 'Em', 'Tom'],
    'region': ['Wales', 'Wales', 'NW England', 'S England']
})
print('\ndf1\n', df1)

df2 = pd.DataFrame({
    'region': ['Wales', 'S England', 'S England', 'NW England', 'NW England'],
    'city':   ['Swansea', 'London', 'Bristol', 'Manchester', 'Leeds']
})
print('\ndf2\n', df2)

# Do a many-to-many merge, based on the common 'region' column. 
df3 = pd.merge(df1, df2)
print('\ndf3\n', df3)

