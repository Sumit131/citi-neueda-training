import numpy as np
import pandas as pd

coords = [ 
    {'x': 1.2, 'y': 11.2},
    {'x': 5.3, 'y': 15.3},
    {'x': 9.4, 'y': 19.4}
]
df = pd.DataFrame(coords)
s  = pd.Series({'x': 100, 'y': 200})

print('Using operators')
print('df + s\n',  df + s)
print('df - s\n',  df - s)
print('df * s\n',  df * s)
print('df / s\n',  df / s)
print('df // s\n', df // s)
print('df % s\n',  df % s)
print('df ** s\n', df ** s)
