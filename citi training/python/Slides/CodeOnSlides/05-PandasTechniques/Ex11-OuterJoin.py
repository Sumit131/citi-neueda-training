import pandas as pd

df1 = pd.DataFrame({
    'name':   ['Andy', 'Jayne', 'Em', 'Tom'],
    'salary': [10000, 20000, 30000, 40000]
})
df1 = df1.set_index('name')
print('\ndf1\n', df1)

df2 = pd.DataFrame({
    'name':  ['Andy', 'Jayne', 'Boris'],
    'level': ['Medium', 'Expert', 'Hopeless'],
    'years': [3, 5, 1]          
})
df2 = df2.set_index('name')
print('\ndf2\n', df2)

# Do an outer join (by setting how='outer').
df3 = pd.merge(df1, df2, left_index=True, right_index=True, how='outer')
df3.index.name = 'name'
print('\ndf3\n', df3)