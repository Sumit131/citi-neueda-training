import pandas as pd

df1 = pd.DataFrame({
    'name':   ['Andy', 'Jayne', 'Em', 'Tom'],
    'office': ['SWAN', 'SWAN', 'MCR', 'LON']
})
print('\ndf1\n', df1)

df2 = pd.DataFrame({
    'office':  ['SWAN', 'LON', 'MCR', 'ABD'],
    'city':    ['Swansea', 'London', 'Manchester', 'Aberdeen'],
    'region':  ['Wales', 'S England', 'NW England', 'Scotland']          
})
print('\ndf2\n', df2)

# Do a many-to-one merge, based on the common 'office' column. 
df3 = pd.merge(df1, df2)
print('\ndf3\n', df3)

