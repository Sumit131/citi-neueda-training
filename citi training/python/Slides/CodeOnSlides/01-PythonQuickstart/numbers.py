i1 = 12345
i2 = 1234567890123456789
i3 = int("123")
print("%d %d %d" % (i1, i2, i3))

f1 = 1.23
f2 = float("123.45")
print("%g %g %g %g" % (f1, f2))

c1 = 1 + 2j
c2 = complex("6+7j")
print("%g + %gi" % (c1.real, c1.imag))
print("%g + %gi" % (c2.real, c2.imag))


 
