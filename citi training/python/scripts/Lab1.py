# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 10:51:56 2021

@author: Administrator
"""


def isleapyear(year):
    if year%4==0:
        if year%100==0: 
            if year%400==0:
                return "True"
        else:
            return "True"
    
    return "False"
    
def getMonthName(month,verbose=False):
    if verbose==True:
            if month==1:  return "January" 
            if month==2:  return "February"
            if month==3:  return "March"
            if month==4:  return "April"
            if month==5:  return "May"
            if month==6:  return "June"
            if month==7:  return "July"
            if month==8:  return "August"
            if month==9:  return "September"
            if month==10: return "October"
            if month==11: return "November"
            if month==12: return "December"
            else: return "Invalid"
            
    else:
            if month==1:  return "Jan" 
            if month==2:  return "Feb"
            if month==3:  return "Mar"
            if month==4:  return "Apr"
            if month==5:  return "May"
            if month==6:  return "June"
            if month==7:  return "July"
            if month==8:  return "Aug"
            if month==9:  return "Sep"
            if month==10: return "Oct"
            if month==11: return "Nov"
            if month==12: return "Dec"
            else: return "Invalid"
            

def getDaySuffix(day):

        if day in [1,21,31]: return "st"
        if day in [2,22]: return "nd"
        if day in [3,23]: return "rd"
        else: return "th"
        
        
if __name__ == "__main__":
    result = isLeapYear (2021)
    print(result)