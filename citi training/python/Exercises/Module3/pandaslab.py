# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 09:32:37 2021

@author: Administrator
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = {'Team':['Brazil','England','France','Italy','Spain','Uruguay','West Germany','Germany','Argentina'],
       'Wins':[5,1,2,4,1,2,3,1,2]}
WCwinners = pd.DataFrame(data)
WCwinners.index.name = 'Index'

WCwinners.to_csv('WorldCupWinners.csv',index = True,header =  True)
df = pd.read_csv('WorldCupWinners.csv',index_col='Index',header=0)

df.to_csv('WorldCupWinners_noheaders.csv',index = False,header =  False)
newdf = pd.read_csv('WorldCupWinners_noheaders.csv',names=['Teams','Wins'])
newdf.index.name='Index'

print(newdf.index)
print(newdf.Teams)
print(newdf.Wins)
print(newdf.describe())

YearFirstWin = pd.Series([1958,1966,1998,1934,2010,1930,1954,2014,1978])
newdf['YearFirstWin'] = YearFirstWin
print(newdf)

newdf = newdf.append({'Teams':'Wales','Wins':1,'YearFirstWin':2022},ignore_index=True)
print(newdf)

print(newdf.loc[newdf['Wins']==1,['Teams','Wins']])
print(newdf.loc[newdf['Wins']>1,['Teams','Wins']])
temp = newdf.loc[newdf['YearFirstWin']>=2000,['Teams','YearFirstWin']].sort_values(by = ['YearFirstWin'],ascending=True)
print(temp.iloc[0])
print(newdf.loc[((newdf['Wins']==2) | (newdf['Wins']==3)),['Teams','Wins']])
print(newdf.loc[((newdf['Wins']==2) | (newdf['Wins']==3)),['Teams']])