# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 11:14:26 2021

@author: Administrator
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv('../../DataFiles/Retail_Asset_List.csv',index_col='Asset ID')
print(pd.concat([df.iloc[:5],df.iloc[-5:]]))
print(df.describe().transpose())
print(df['Brand'].nunique())
print(df['Location'].nunique())
print(df['Province'].nunique())
print(df['Asset Type'].nunique())
print(df['Brand'].unique())
print(df['Asset Type'].unique())

print(df.loc[df['Location']=='China'].count())
print(df.loc[df['Employees']>500].count())
print(df.loc[df['Location'].isin(['Vietnam','United States','China']),['Asset Type','City','Brand']])

temp = df.loc[df['Employees']>1000,['Employees']]
plt.bar(temp.index,temp['Employees'].tolist(),width=500)
plt.savefig('Most Employee.png')
plt.show()
