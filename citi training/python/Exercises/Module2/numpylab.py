# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 09:29:11 2021

@author: Administrator
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
df=pd.read_excel('BergenWeather2019.xlsx',dtype=np.float32)

rain = np.array(df['Precipitation'])

print(rain[0])
print(rain[1])
print(rain[-1])
print(rain[-2])
print(rain[:31])
print(rain[-31:])
print(rain[-61:-31])
print(rain[::10])

rain2d = rain[:364].reshape(52,7)

print(rain2d[0])
print(rain2d[-1])
print(rain2d[0][-2:])
print(rain2d[-1][:5])

import matplotlib.pyplot as plt
x=np.arange(1,366)
plt.plot(x,rain)
plt.show()
plt.bar(x,rain)
plt.show()
plt.hist(rain)
plt.show()