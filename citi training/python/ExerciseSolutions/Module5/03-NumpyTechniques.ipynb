{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Overview\n",
    "In this lab you’ll continue working with the Bergen 2019 weather dataset. You’ll make use of universal functions, calculate aggregations, explore broadcasting, and dig deeper into the meaning of the data by using Boolean logic. If time permits, you’ll get a chance to try out fancy indexing, partitioning, and sorting.\n",
    "\n",
    "## Roadmap\n",
    "There are 8 exercises in this lab, of which the last two exercises are \"if time permits\". Here is a brief summary of the tasks you will perform in each exercise; more detailed instructions follow later:\n",
    "1.\tUsing universal operators and functions\n",
    "2.\tSpecifying the data type for elements in a NumPy array\n",
    "3.\tCreating a multidimensional NumPy array\n",
    "4.\tWorking with a multidimensional NumPy array\n",
    "5.\tMaking use of NumPy broadcasting capabilities\n",
    "6.\tUsing Boolean operations and aggregation on an array \n",
    "7.\t(If time permits) Using Boolean masks on an array\n",
    "8.\t(If time permits) Additional suggestions\n",
    "\n",
    "\n",
    "# Familiarization\n",
    "In the student folder, open processBergenData.py in a text editor. This file loads the Bergen 2019 weather data from a CSV file into a Pandas DataFrame object, and then assigns the precipitation column into a NumPy array. The NumPy array has shape (365,).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Global Settings"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "csv_file = './Data/BergenWeather2019.csv'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 1:  Using universal operators and functions\n",
    "\n",
    "Universal operators and functions are NumPy array methods that automatically execute on each element in the array, without the need for you to write explicit looping code. Universal functions are more concise than loops, and far more efficient.\n",
    "\n",
    "Using universal operators, convert all the precipitation values from millimetres into the following units. In each case, print out the new array values:\n",
    "-\tCentimetres (to convert from mm to cm, divide by 10)\n",
    "-\tInches (to convert from cm to inches, divide by 2.54)\n",
    "\n",
    "The inches values will be displayed to machine accuracy, e.g. 0.99212598. You don’t need this much accuracy; 4 decimal places is perfectly adequate. Think about how you can convert the values to 4 decimal places; if you’re thinking of using a loop, think again. You hardly ever use loops with NumPy arrays, because they are grossly inefficient. Instead, your first though should be “I wonder if NumPy has a universal function to do that…”. As it happens, NumPy has an around() universal function to round all values in an array to the specified number of decimal places. See here for details:\n",
    "https://docs.scipy.org/doc/numpy/reference/generated/numpy.around.html\n",
    "Use the around() function to round all the inches values to 4 decimal places, then print the array.\n",
    "\n",
    "When you’ve got all that working, combine your operations into a single statement that does all of the following:\n",
    "- Converts the precipitation values from mm to inches, to 4 decimal places.\n",
    "You can write arbitrarily complex statements using universal operators and functions 😊\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read a csv file, get a Pandas DataFrame back, and assign the 'Prediction' column to a NumPy array.\n",
    "dataframe = pd.read_csv(csv_file)\n",
    "precipitation_mm = np.array(dataframe['Precipitation'])\n",
    "print('\\nPrecipitation mm\\n', precipitation_mm)\n",
    "\n",
    "precipitation_cm = precipitation_mm / 10\n",
    "print('\\nPrecipitation cm\\n', precipitation_cm)\n",
    "\n",
    "precipitation_inches = precipitation_cm / 2.54\n",
    "print('\\nPrecipitation inches\\n', precipitation_inches)\n",
    "\n",
    "precipitation_inches_4dp = np.around(precipitation_inches, 4)\n",
    "print('\\nPrecipitation inches to 4dp\\n', precipitation_inches_4dp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 2:  Calculating aggregations\n",
    "\n",
    "NumPy has a wealth of functions for calculating aggregation results. Aggregations are very important in data science, and you’ll use these functions a lot.\n",
    "\n",
    "Using the precipitation data in inches (to 4dp), output the following aggregation information. Where indicated, round the result to 4dp via the round() standard Python function (why is it OK to use round() here, rather than the NumPy around() function?).\n",
    "-\tTotal annual precipitation, to 4dp (correct answer is 92.3893)\n",
    "-\tMinimum precipitation on a day (correct answer is 0.0, as you might guess 😊)\n",
    "-\tMaximum precipitation on a day (correct answer is 3.622)\n",
    "-\tMean daily precipitation, to 4dp (correct answer is 0.2531)\n",
    "-\tMedian daily precipitation, to 4dp (correct answer is 0.0669)\n",
    "-\tVariance, to 4dp (correct answer is 0.1952)\n",
    "-\tStandard deviation, to 4dp (correct answer is 0.4418)\n",
    "-\tThe quartiles (i.e. 25th percentile, 50th percentile, and 75th percentile). The correct answers are 0.0, 0.0669, and 0.315 respectively. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Total annual precipitation:    ', round(np.sum(precipitation_inches_4dp), 4))\n",
    "print('Minimum precipitation on a day:', np.min(precipitation_inches_4dp))\n",
    "print('Maximum precipitation on a day:', np.max(precipitation_inches_4dp))\n",
    "print('Mean daily precipitation:      ', round(np.mean(precipitation_inches_4dp), 4))\n",
    "print('Median daily precipitation:    ', round(np.median(precipitation_inches_4dp), 4))\n",
    "print('Variance:                      ', round(np.var(precipitation_inches_4dp), 4))\n",
    "print('Standard deviation:            ', round(np.std(precipitation_inches_4dp), 4))\n",
    "print('1st quartile:                  ', np.percentile(precipitation_inches_4dp, 25))\n",
    "print('2nd quartile:                  ', np.percentile(precipitation_inches_4dp, 50))\n",
    "print('3rd quartile:                  ', np.percentile(precipitation_inches_4dp, 75))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 3:  Creating a multidimensional NumPy array\n",
    "\n",
    "In Exercises 3 to 5, you’ll see how to create and use a multidimensional NumPy array. The array will have shape (365,2) and will contain the daily minimum temperatures (in column 0) and the daily maximum temperatures (in column 1).\n",
    "\n",
    "At the moment, your code reads the data into a Pandas DataFrame and then plucks out the 'Precipitation' column into a NumPy array. The code looks like this:\n",
    "\n",
    "`precipitation = np.array(dataframe['Precipitation'])`\n",
    "\n",
    "It’s also possible to pluck out multiple columns from a Pandas DataFrame – you just specify an array of column names. You can then convert the multiple columns into a 2D NumPy array via the to_numpy() function. For example, the following code plucks out the 'MinTemp' and 'MaxTemp' columns and converts them into a 2D NumPy array:\n",
    "\n",
    "`temps = dataframe[['MinTemp', 'MaxTemp']].to_numpy()`\n",
    "\n",
    "Print out the shape of the temps array. It should be (365,2).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the 'MinTemp' and 'MaxTemp' columns into a NumPy array of shape (365,2).\n",
    "temps = dataframe[['MinTemp', 'MaxTemp']].to_numpy()\n",
    "print('temps shape', temps.shape)\n",
    "print('temps dtype', temps.dtype)\n",
    "print(temps)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 4:  Working with a multidimensional NumPy array\n",
    "\n",
    "Create an array named diurnal_ranges that contains the diurnal temperature range for each day (i.e. the maximum temperature minus the minimum temperature for each day). The diurnal_ranges array will have shape (365,).\n",
    "\n",
    "Using universal operators, get the following subarrays and print them:\n",
    "-\tDiurnal ranges > 8 degrees Celsius\n",
    "-\tDiurnal ranges < 4 degrees Celsius\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Work with a multidimensional NumPy array.\n",
    "diurnal_ranges = temps[:, 1] - temps[:, 0]\n",
    "print('\\nDiurnal temperature ranges:\\n', diurnal_ranges)\n",
    "print('\\nDiurnal temperature range > 8.0?\\n', diurnal_ranges > 8.0)\n",
    "print('\\nDiurnal temperature range < 4.0?\\n', diurnal_ranges < 4.0)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 5:  Making use of NumPy broadcasting capabilities\n",
    "\n",
    "In this exercise you’ll use NumPy broadcasting capabilities to perform binary operations on arrays of different shape. You might like to take a moment to remind yourself about how broadcasting works before you tackle this exercise.\n",
    "\n",
    "The first step in this exercise is to calculate the following values:\n",
    "-\tThe mean value in the “minimum temperature” column in the temps array\n",
    "-\tThe mean value in the “maximum temperature” column in the temps array\n",
    "\n",
    "Now create a NumPy array containing these two values. Print the array values – they should be [6.0 12.4], i.e. the mean minimum temperature over the year was 6, and the mean maximum temperature over the year was 12.4. Also print the array shape – it should be (2,).\n",
    "\n",
    "The next step is to see how the temperatures for each day compared to these means. For example, consider the temperatures for the first day of the year:\n",
    "-\tFor the first day of the year, the minimum temperature is 3.1. Compared to the mean minimum temperature of 6 for the whole year, this day is 2.9 cooler.\n",
    "-\tFor the first day of the year, the maximum temperature is 9.7. Compared to the mean maximum temperature of 12.4 for the whole year, this day is 2.7 cooler.\n",
    "-\tThus, the result we’d like to obtain for the first day is [-2.9 -2.7].\n",
    "\n",
    "Now think how you can perform this calculation for every day of the year – i.e. take the temps array of shape (365,2) and subtract the array containing mean min and max temps of shape (2,). These arrays have different shape, but NumPy’s broadcasting rules allows you to do it.\n",
    "Get this working and be sure you understand what’s happening. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "# Determine the average in column 0 (min temps) and the average in column 1 (max temsp).\n",
    "mean_mintemp = round(np.mean(temps[:, 0]), 1)\n",
    "mean_maxtemp = round(np.mean(temps[:, 1]), 1)\n",
    "print('Mean min temp:', mean_mintemp)\n",
    "print('Mean max temp:', mean_maxtemp)\n",
    "\n",
    "# Create a NumPy 1D array of shape (2,), containing averages for min and max temps.\n",
    "mean_mintemp_maxtemp = np.array([mean_mintemp, mean_maxtemp])\n",
    "print('Array containing mean min and mean max temps:', mean_mintemp_maxtemp)\n",
    "print('Shape:', mean_mintemp_maxtemp.shape)\n",
    "\n",
    "# Make use of NumPy \"broadcasting\" to do maths on arrays of shapes (52,2) and (2,).  \n",
    "print('\\nMinTemp relative to mean_mintemp, and MaxTemp relative to mean_maxtemp:\\n', temps - mean_mintemp_maxtemp)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 6:  Using Boolean operations and aggregation on an array\n",
    "\n",
    "For the remainder of the lab, you’ll switch back to using the precipitation data, i.e. the 1D array of shape (365,).\n",
    "\n",
    "Perform the following Boolean operations on the precipitation data. In each case, the result is a 1D array of shape (365,) containing True or False values for each day:\n",
    "-\tWas there any precipitation on a day?\n",
    "-\tWas there between 1 inch and 2 inches of precipitation on a day?\n",
    "\n",
    "Now perform the following Boolean aggregation on the precipitation data:\n",
    "-\tWere there any days with more than 2 inches of precipitation? The result should be a single Boolean, i.e. True of False, that represents the overall result for the whole year.\n",
    "-\tHow many days had more than 2 inches of precipitation during the whole year? The result should be an integer (the correct answer is 4 days).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "precipitation = np.array(dataframe['Precipitation'])\n",
    "precipitation = np.around(precipitation / (10 * 2.54), 4)\n",
    "\n",
    "print('\\nPrecipitation on this day?:\\n', precipitation > 0)\n",
    "print('\\n1 to 2 inces of precipitation on this day?:\\n', (precipitation >= 1) & (precipitation <= 2))\n",
    "\n",
    "# Use Boolean aggregation on a NumPy array.\n",
    "print('Were there any days with more than 2 inches of precipitation?', np.any(precipitation > 2))\n",
    "print('How many days had more than 2 inches of precipitation?:      ', np.count_nonzero(precipitation > 2))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 7 (If time permits):  Using Boolean masks on an array\n",
    "\n",
    "Use a Boolean mask on the precipitation data to get an array of days of precipitation, i.e. just the days where the precipitation is nonzero. Print out the following info (NumPy arrays have a size property you can use here):\n",
    "-\tThe number of days of precipitation (the correct answer is 258)\n",
    "-\tThe number of days of no precipitation (the correct answer is 107)\n",
    "\n",
    "Now imagine a city has 365 inches of rain across the whole year, but bizarrely it all falls on a single day. The mean rainfall across the whole year is 1 inch per day, but the mean rainfall across just the rainy days is 365 inches. As you can see, eliminating zero-precipitation days can give a different insight into the data.\n",
    "\n",
    "With this in mind, print the following information to see precipitation statistics across the whole year, compared to precipitation statistics on just the rainy days:\n",
    "-\tMean daily precipitation all year, compared to that of rainy days only.\n",
    "-\tMedian daily precipitation all year, compared to that of rainy days only.\n",
    "-\tVariance all year, compared to that of rainy days only.\n",
    "-\tStandard deviation all year, compared to that of rainy days only.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "# Use a Boolean mask on a NumPy array, to get an array of days of precipitation.\n",
    "days_of_precipitation = precipitation[precipitation > 0]\n",
    "print('\\nNumber of days of precipitation:   ', days_of_precipitation.size)\n",
    "print('Number of days of no precipitation:', precipitation.size - days_of_precipitation.size)\n",
    "print('\\nPrecipitation amounts:\\n', days_of_precipitation)\n",
    "\n",
    "# Compare the mean daily precipitation all year, with that of rainy days only.\n",
    "print('\\nMean daily precipitation all year:  ', round(np.mean(precipitation), 4))\n",
    "print('Mean of rainy days only:            ',   round(np.mean(days_of_precipitation), 4))\n",
    "\n",
    "# Compare the median daily precipitation all year, with that of rainy days only.\n",
    "print('\\nMedian daily precipitation all year:', round(np.median(precipitation), 4))\n",
    "print('Median of rainy days only:          ',   round(np.median(days_of_precipitation), 4))\n",
    "\n",
    "# Compare the variance of daily precipitation all year, with that of rainy days only.\n",
    "print('\\nVariance all year:                  ', round(np.var(precipitation), 4))\n",
    "print('Variance of rainy days only:        ',   round(np.var(days_of_precipitation), 4))\n",
    "\n",
    "# Compare the standard deviation of daily precipitation all year, with that of rainy days only.\n",
    "print('\\nStd dev all year:                   ', round(np.std(precipitation), 4))\n",
    "print('Std dev of rainy days only:         ',   round(np.std(days_of_precipitation), 4))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 8 (If time permits):  Additional suggestions\n",
    "\n",
    "-\tUse fancy indexing to pluck out the first and last days of precipitation.\n",
    "-\tPartition the rainy days into the 30 least wet days, then the rest.\n",
    "-\tSort the rainy days.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "days_of_precipitation = precipitation[precipitation > 0]\n",
    "print('\\nPrecipitation amounts:\\n', days_of_precipitation)\n",
    "\n",
    "# Use fancy indexing to pluck out the first and last days of precipitation.\n",
    "first_last_wet_days = [0, -1]\n",
    "precipitation_first_last_wet_days = days_of_precipitation[first_last_wet_days]\n",
    "print('\\nPrecipitation on first and last wet days:', precipitation_first_last_wet_days)\n",
    "\n",
    "# Partition the rainy days into the 30 least wet days, then the rest.\n",
    "print('\\nPrecipitation partitioned at 30:\\n',   np.partition(days_of_precipitation, 30))\n",
    "\n",
    "# Sort the rainy days.\n",
    "print('\\nPrecipitation in sorted order:\\n', np.sort(days_of_precipitation))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
