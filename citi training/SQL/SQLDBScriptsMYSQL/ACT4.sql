-- ***** Subqueries and Views *****
-- 1. Use a subquery to generate a list of customers (CompanyName, ContactName, Country, City) 
-- that are in the same country as 
-- the employees.
select * from customers;
select * from employees;
Select CompanyName, ContactName, c.Country, c.City 
from customers c join employees e on c.country = e.country;
-- 2.  Execute this statement:
SELECT ProductName, DATE_FORMAT(OrderDate, "%D %b %Y") `Order Date`, Quantity 
FROM Products p 
JOIN Order_Details od ON p.ProductID = od.ProductID 
JOIN Orders o ON o.OrderID = od.OrderID 
ORDER BY ProductName, Quantity DESC;
-- Can you modify it so that we only see the row or rows (in the case of a tie) for the best sale for each product?
SELECT ProductName, DATE_FORMAT(OrderDate, "%D %b %Y") `Order Date`, Quantity 
FROM Products p 
JOIN Order_Details od ON p.ProductID = od.ProductID 
JOIN Orders o ON o.OrderID = od.OrderID 
WHERE Quantity =
(
	SELECT MAX(Quantity) FROM order_details Where ProductID = p.ProductID
)
ORDER BY ProductName, Quantity DESC;
-- 3. Create a view vCountryFromTo that lists the OrderID(Orders), OrderDate(Orders), ProductName(Products), 
-- Country(Suppliers) with an alias
-- 'Supplier Country' and Country(Customers) with an alias 'Customer Country'.  You will need to join the 
-- Suppliers, Products, Order_Details,
-- Orders and Customers tables to do this.  When querying the view you can limit the results to a single 
-- month of your choice.
select * from suppliers;
select * from products;
select * from order_details;
select * from orders;
select * from customers;
CREATE view vCountryFromTo AS
SELECT o.OrderID, OrderDate, ProductName, s.Country 'Supplier Country', c.Country 'Customer Country'
FROM suppliers s
JOIN products p ON s.supplierID = p.supplierID
JOIN order_details od ON p.productID = od.productID
JOIN orders o on od.orderID = o.orderID
Join customers c on o.customerid = c.customerid;
SELECT * FROM vCountryFromTo WHERE MONTH(OrderDate) = 07;








