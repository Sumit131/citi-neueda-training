-- **** Select Statements Exercise ****
-- 1 Show the ProductName and UnitPrice of all products in the price range 20 to 30
use northwind;
select productname,unitprice from products where unitprice between 20 and 30 order by unitprice;
-- 2 Show the countries in the customers table with no duplicates
select distinct country from customers;
-- 3 Show the CompanyName, Country and City of all customers that are restaurants (the word restaurant should be in the CompanyName)
select CompanyName, Country, City from customers where companyname like '%restaurant%';
-- 4 Show The CompanyName, ContactName, Country,and City of all suppliers from Germany, France, Italy and Spain
select CompanyName, ContactName, Country, City from suppliers where country in ('Germany', 'France', 'Italy','Spain');
-- 5 Show the OrderID, OrderDate and CustomerID for all orders in July of 2017
select orderid,orderdate,customerid from orders where year(orderdate)=2017 and month(orderdate)=07;
-- 6 Create a report that shows each ProductName, UnitPrice, UnitsInStock and ReorderLevel and a message
-- for each product that will be either 'Order Stock' (UnitsInStock is equal to or below ReorderLevel) or 'Sufficient Stock' 
-- (UnitsInStock is above the ReorderLevel) give the report headings such as Product, Price, Stock, 'Reorder Level' and 
-- 'Stock Alert'.  Discontinued products should be excluded from the report.
select * from products;

select ProductName as product, UnitPrice as price, UnitsInStock as stock, ReorderLevel, 
case 
    when unitsinstock <= reorderlevel then "order stock"
    else "Sufficient stock"
end 'stock alert'
from products where discontinued = 0;

