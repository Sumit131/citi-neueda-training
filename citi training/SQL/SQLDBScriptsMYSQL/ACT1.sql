-- **** Databases, Tables and CRUD Operations ****
-- 1. Create a database called coursedb
create database coursedb;
use coursedb;
-- 2. In the coursedb database create a table called suppliers with these fields:
-- supplierid int (Primary Key), company varchar(40), country varchar(30), city varchar(30) 
create table suppliers(
	supplierid int Primary Key, 
    company varchar(40), 
    country varchar(30), 
    city varchar(30)
);
-- 3. Execute this statement to populate the suppliers table

insert into suppliers(supplierid, company, country, city) 
select SupplierID, CompanyName, Country, City from northwind.Suppliers;

-- 4. Create another table called products with these fields:
-- productid int auto_increment (Primary Key), product varchar(40), price decimal(6,2), 
-- supplierid int (Foreign key that references supplierid in the suppliers table)
create table products(
	productid int auto_increment Primary Key,
	product varchar(40), 
    price decimal(6,2), 
	supplierid int, 
    Foreign key(supplierid) references suppliers(supplierid)
);
-- 5. Execute this statement to populate the table

insert into products(product, price, supplierid) 
select productname, unitprice, supplierid from northwind.products;

-- 6.  Try some insert, update and delete statements, you should not be able to delete existing suppliers due
-- to the foreign key constraint but you can delete newly inserted suppliers and any existing products.  The foreign 
-- key constraint will also prevent you from changing the supplierid of a product to one that does not exist in the 
-- suppliers table

select * from products;
Insert into products(product,price,supplierid) values('chessesticks','100','8');
delete from products where productid = 130;