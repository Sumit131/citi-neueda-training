DROP DATABASE IF EXISTS TradesDB;
CREATE DATABASE TradesDB;
USE TradesDB;


CREATE TABLE trade
(
  ID INT NOT NULL,
  instant DATETIME NOT NULL,
  stock VARCHAR(8) NOT NULL,
  buy BIT NOT NULL,
  size INT NOT NULL,
  price DECIMAL(12,4) NOT NULL,
  PRIMARY KEY (ID)
);

CREATE TABLE trader
(
  ID INT NOT NULL,
  first_name VARCHAR(24) NOT NULL,
  last_name VARCHAR(24) NOT NULL,
  PRIMARY KEY (ID)
);

CREATE TABLE position
(
  ID INT NOT NULL,
  trader_ID INT NOT NULL,
  opening_trade_ID INT NOT NULL,
  closing_trade_ID INT,
  PRIMARY KEY (ID),
  FOREIGN KEY (trader_ID) REFERENCES trader (ID),
  FOREIGN KEY (opening_trade_ID) REFERENCES trade (ID),
  FOREIGN KEY (closing_trade_ID) REFERENCES trade (ID)
);

CREATE TABLE price_point
(
  ID INT,
  stock VARCHAR(8) NOT NULL,
  instant DATETIME NOT NULL,
  opn DECIMAL(12,4) NOT NULL,
  high DECIMAL(12,4) NOT NULL,
  low DECIMAL(12,4) NOT NULL,
  cls DECIMAL(12,4) NOT NULL,
  volume INT NOT NULL,
  PRIMARY KEY (ID)
);
