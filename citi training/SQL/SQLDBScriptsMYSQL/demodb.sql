USE northwind;

Select * from Shippers;

Create database Coursedemo;
Use Coursedemo;
drop table Contact;
drop table Country;
CREATE TABLE Country(
	id int auto_increment primary key,
    country_name varchar(100) not null,
    continent varchar(20) not null
);

Insert Country(country_name,continent) values('india','asia');
Select * from Country;

create table Contact(
	id int auto_increment primary key,
    contact_name varchar(30) not null,
    email varchar(40) unique not null,
    countryid int,
    foreign key(countryid) references Country(id)
);
use coursedemo;

Insert into Country(country_name,continent) select Name,Continent from world.country;
INSERT INTO Contact(contact_name, email, countryid) VALUES('Virat Kohli', 'virat@gmail.com', 1);

USE world;
select * from country;

Use northwind;
Select * from Shippers;
insert shippers(Companyname,Phone) values ('mumbai couriers','696969652');
Update shippers set Phone='8964651564' where ShipperID = 4; 
delete from shippers where shipperid=4;

-- yoo hoo

select companyname, contactname, country, city from suppliers
where country not in ('Germany', 'France', 'Spain');

Select orderid,orderdate from orders where orderdate <="2017-01-01";

Select orderid,orderdate from orders where orderdate <"2016-09-01"
and orderdate > "2016-07-31";

Select orderid,orderdate from orders where orderdate between "2016-07-31" and "2016-09-01";

Select orderid,orderdate from orders where year(orderdate)=2016 and month(orderdate) = 08;

select companyname from customers where companyname like '%Market%';

SELECT CustomerID, CompanyName FROM Customers WHERE CustomerID LIKE '_N___';

insert shippers(companyname,phone) values ("another shippers","65468464");
delete from shippers where shipperid=6;
select * from shippers ;

select productname,categoryid,unitprice from products 
order by categoryid,unitprice DESC;  -- very useful

Select distinct country,city from suppliers order by country;

SELECT CategoryName, 
	CASE CategoryName 
		WHEN 'Beverages' THEN 'Drinks' 
		WHEN 'Condiments' THEN 'Seasonings' 
		WHEN 'Confections' THEN 'Sweets' 
		ELSE 'Something Else' 
	END Comment 
FROM Categories;

SELECT ProductName, UnitPrice, 
CASE 
	WHEN UnitPrice > 40 THEN 'Expensive' 
	WHEN UnitPrice > 20 THEN 'Reasonable' 
	ELSE 'Cheap' END Comment 
FROM Products;

-- joins and aggregates

select companyname, orderid, orderdate from customers c
join orders o on c.customerid = o.customerid 
where country = 'germany';

SELECT CompanyName, OrderID, OrderDate, CONCAT(FirstName,' ',LastName) `Sales Person` 
FROM Customers c
JOIN Orders o ON c.CustomerID = o.CustomerID
JOIN Employees e ON o.EmployeeID = e.EmployeeID
WHERE c.Country = 'Germany';


select s.companyname suppliers, s.country 'Supplier Country',
c.companyname customer, c.country 'Customer Country' 
from suppliers s right join customers c on s.country=c.country;

select categoryname, productname from categories, products order by categoryname;

SELECT * FROM Employees;
SELECT e.LastName Employee, e.Title, m.LastName Manager, m.Title FROM 
Employees e JOIN Employees m;

select companyname,contactname from customers
union
select companyname,contactname from suppliers;

select productname, unitprice from products order by unitprice desc limit 10;

select sum(quantity) 'Total sales' from order_details;
select productname, sum(quantity) 'Total sales' from order_details od
join products p on od.productid=p.productid
group by productname;
select productname, sum(quantity) 'Total sales' from order_details od
join products p on od.productid=p.productid
group by productname having sum(quantity)>1000 order by sum(quantity);

USE Northwind;
SELECT CategoryName, ProductName, 
SUM(Quantity) AS Sales
FROM Products p JOIN Order_Details od
ON p.ProductID = od.ProductID JOIN Categories c
ON c.CategoryID = p.CategoryID
GROUP BY CategoryName, ProductName
WITH ROLLUP;

-- 2.  Execute this statement:
SELECT ProductName, DATE_FORMAT(OrderDate, "%D %b %Y") `Order Date`, Quantity 
FROM Products p 
JOIN Order_Details od ON p.ProductID = od.ProductID 
JOIN Orders o ON o.OrderID = od.OrderID 
ORDER BY ProductName, Quantity DESC;
-- Can you modify it so that we only see the row or rows (in the case of a tie) for the best sale for each product?

SELECT ProductName, DATE_FORMAT(OrderDate, "%D %b %Y") `Order Date`, Quantity 
FROM Products p 
JOIN Order_Details od ON p.ProductID = od.ProductID 
JOIN Orders o ON o.OrderID = od.OrderID 
WHERE Quantity =
(
	SELECT MAX(Quantity) FROM order_details Where ProductID = p.ProductID
)
ORDER BY ProductName, Quantity DESC;
