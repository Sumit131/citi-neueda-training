import pandas as pd
# Get Salary Data
df = pd.read_csv('http://neueda.conygre.com/pydata/ml_fc/demo2_data.csv')

# Visualize the data
%matplotlib inline
from pandas.plotting import scatter_matrix
scatter_matrix(df)

# This must be a matrix note the naming convention
X = df[ ['YearsExperience'] ]
y = df['Salary']

# Import LinearRegression and use that model
from sklearn.linear_model import LinearRegression
model = LinearRegression

# Pass the data into the model
model.fit(X, y)

# Check some parameters about the data
print("Model coef_      %f" % model.coef_)
print("Model intercept_ %f" % model.intercept_)

# Make some predictions
model.predict([[4],[5],[6]])

# We will go back and split the data into training and testing data

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

display(X_train)
display(X_test)

# Change model.fit to
model.fit(X_train, y_train)

# Make predictions look at the test and train data and the plotted line
import matplotlib.pyplot as plt

plt.plot(X_train, model.predict(X_train), color='blue')

plt.scatter(X_train, y_train, color='red')

plt.scatter(X_test, y_test, color='green')

plt.show()
