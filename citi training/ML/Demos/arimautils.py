import matplotlib.pyplot as plt
# from statsmodels.tsa.arima_model import ARIMA
from statsmodels.tsa.arima.model import ARIMA

def fit_and_plot_ARIMA_model(data, differenced_data, p, d, q, subplot_pos) :
    
    # Use ARIMA to build a model for the data.
    model = ARIMA(data, order=(p, d, q))  

    # Fit the model to the data.
    results = model.fit()
    
    # Calculate the residual sum of squares.
    rss = sum((results.fittedvalues - differenced_data) ** 2)

    # Plot differenced data, fitted (predicted) values, and RSS error indicator.
    plt.subplot(subplot_pos)
    plt.plot(differenced_data)
    plt.plot(results.fittedvalues, color='orange')
    plt.title('ARIMA(%d, %d, %d) rss=%.4f' % (p, d, q, rss))
