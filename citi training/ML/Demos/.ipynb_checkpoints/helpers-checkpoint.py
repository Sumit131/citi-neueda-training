import matplotlib.pyplot as plt

def draw_digits(digits, image_base_index = 0, y_pred = None, y_test = None) :

    # Create a figure and a set of subplots. We'll draw each digit in a separate subplot.
    # The function returns a handle to the Figure object created, plus a 10x10 array of Axes objects.
    # For full details, see https://matplotlib.org/api/_as_gen/matplotlib.pyplot.subplots.html#matplotlib.pyplot.subplots.
    fig, axes = plt.subplots(10, 10,                                     # We'll be plotting 10 rows, 10 columns.  
                             figsize=(7, 7),                             # The whole figure will be 7 x 7 inches.
                             subplot_kw={'xticks':[], 'yticks':[]})      # Don't show any ticks on the axes (it would be too messy).

    # Flatten the 10x10 array of Axes objects into a linear 100-element array of Axes objects, and enumerate over them. We'll draw a separate image on each Axes object.   
    for i, ax in enumerate(axes.flat):
               
        # Call the Axes object's imshow() method, to display an image. 
        ax.imshow(digits.images[i + image_base_index],           # The image to show. 
                  cmap='binary')                                 # Colour map for pixels, binary (i.e. black and white).
                  
        if (y_pred is None) or (y_test is None):
            text_label = str(digits.target[i + image_base_index])
            text_color = 'green'
        else:
            text_label = str(y_pred[i])
            text_color = 'green' if y_pred[i] == y_test[i] else 'red'
 
        # Draw          
        ax.text(0.05, 0.05, text_label, transform=ax.transAxes, color=text_color)

    # Show the whole figure (at last!)
    plt.show()
